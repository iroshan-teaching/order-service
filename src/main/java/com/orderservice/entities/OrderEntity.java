package com.orderservice.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class OrderEntity {

    @Id
    private Long id;
    private String orderId;
    private Long userId;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getOrderId() {return orderId;}

    public void setOrderId(String orderId) {this.orderId = orderId;}

    public Long getUserId() {return userId;}

    public void setUserId(Long userId) {this.userId = userId;}
}
