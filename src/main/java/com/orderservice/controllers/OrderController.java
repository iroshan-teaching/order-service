package com.orderservice.controllers;

import com.orderservice.dots.OrderDTO;
import com.orderservice.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * ============================================================
 * This controller is responsible for managing all the api
 * requests with respect to order.
 * ============================================================
 */
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    // Save order

    /**
     * ========================================================
     * This method is used to return all the order data by user.
     * ========================================================
     * @return
     */
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id) {
        return orderService.getOrdersByUserId(id);
    }
}
