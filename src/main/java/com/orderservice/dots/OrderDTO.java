package com.orderservice.dots;

public class OrderDTO {
    private String id;
    private String orderId;
    private Long userId;

    public OrderDTO(String id) {}

    public OrderDTO(String id, String orderId, Long userId) {
        this.id = id;
        this.orderId = orderId;
        this.userId = userId;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getOrderId() {return orderId;}

    public void setOrderId(String orderId) {this.orderId = orderId;}

    public Long getUserId() {return userId;}

    public void setUserId(Long userId) {this.userId = userId;}
}
