package com.orderservice.services;

import com.orderservice.dots.OrderDTO;
import com.orderservice.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ============================================================
 * This service is responsible for managing all the business
 * logic related to order.
 * ============================================================
 */
@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository repository;

    /**
     * ========================================================
     * This method is used to retrieve all order data by usrId.
     * ========================================================
     * @return
     */
    public List<OrderDTO> getOrdersByUserId(long id) {
        List<OrderDTO> users =  repository.findOrdersByUserId(id)
                .stream()
                .map(order -> new OrderDTO(
                        order.getId().toString(),
                        order.getOrderId(),
                        order.getUserId()
                )).collect(Collectors.toList());
        return users;
    }
}
